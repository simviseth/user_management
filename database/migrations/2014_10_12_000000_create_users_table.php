<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        /*Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();*/

            Schema::create('user', function (Blueprint $table) {
                $table->id('user_id');
                $table->unsignedBigInteger('role_id');
                $table->string('username');
                $table->string('password');
                $table->string('email');
                $table->foreign('role_id')->references('role_id')->on('role');
                
                $table->timestamps();
            });
    }
    public function down(): void
    {
        Schema::dropIfExists('user');
    }
    
};
